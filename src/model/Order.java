package model;

import errorHandler.OrderExceptionHandler;
import errorHandler.ProductExceptionHandler;

public class Order {
	
	private int id;
	private int customer_id;
	private int product_id;
	private int area_id;
	private String status;
	private String order_date;
	private String update_date;
	private String billing_date;
	private String create_at;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	public int getArea_id() {
		return area_id;
	}
	public void setArea_id(int area_id) {
		this.area_id = area_id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOrder_date() {
		return order_date;
	}
	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}
	public String getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	public String getBilling_date() {
		return billing_date;
	}
	public void setBilling_date(String billing_date) {
		this.billing_date = billing_date;
	}
	public String getCreate_at() {
		return create_at;
	}
	public void setCreate_at(String create_at) {
		this.create_at = create_at;
	}
	public Order(int customer_id, int product_id, int area_id, String status, String order_date,
			String update_date, String billing_date, String create_at) {
		

		try {
			
			validateCustomerID(customer_id);
			validateProductID(product_id);
			validateAreaID(area_id);
		} catch (OrderExceptionHandler e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.customer_id = customer_id;
		this.product_id = product_id;
		this.area_id = area_id;
		this.status = status;
		this.order_date = order_date;
		this.update_date = update_date;
		this.billing_date = billing_date;
		this.create_at = create_at;
	}
	
	public static void validateCustomerID(int customer_id) throws OrderExceptionHandler {
		// TODO Auto-generated method stub
		if (customer_id <= 0)
			throw new OrderExceptionHandler("customer id is less than or equal to zero");	
		
	}
	
	public static void validateProductID(int product_id) throws OrderExceptionHandler {
		// TODO Auto-generated method stub
		if (product_id <= 0)
			throw new OrderExceptionHandler("product id is less than or equal to zero");		
	}
	
	
	public static void validateAreaID(int area_id) throws OrderExceptionHandler {
		// TODO Auto-generated method stub
		if (area_id <= 0)
			throw new OrderExceptionHandler("area id is less than or equal to zero");
				
	}
	
	

}
