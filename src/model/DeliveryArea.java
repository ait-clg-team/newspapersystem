package model;

import errorHandler.DeliveryExceptionHandler;

public class DeliveryArea {
	
	String DA_country;
	String DA_town;
	int DA_id;
	String DA_view;
	
	
	public String DA_view() {
		return DA_country + DA_town + DA_id;
	}
	
	
	public String getDA_view() {
		return DA_view;
	}


	public void setDA_view(String dA_view) {
		DA_view = dA_view;
	}


	public String getDA_country() {
		return DA_country;
	}
	
	

	public void setDA_country(String dA_country) {
		DA_country = dA_country;
	}

	public String getDA_town() {
		return DA_town;
	}

	public void setDA_town(String dA_town) {
		DA_town = dA_town;
	}

	public int getDA_id() {
		return DA_id;
	}

	public void setDA_id(int dA_id) {
		DA_id = dA_id;
	}

	
	public static void validateDeliveryAreaCountry(String DeliveryAreaCountry) throws DeliveryExceptionHandler {
		// TODO Auto-generated method stub
		if (DeliveryAreaCountry.isBlank() || DeliveryAreaCountry.isEmpty())
			throw new DeliveryExceptionHandler("Delivery Area Country NOT specified");
		else if (DeliveryAreaCountry.length() > 20)
			throw new DeliveryExceptionHandler("Delivery Area Country does not meet maximum length requirements");
		else if (DeliveryAreaCountry.length() < 3)
			throw new DeliveryExceptionHandler("Delivery Area Country does not meet minimum length requirements");
		
	}
	
	public static void validateDeliveryAreaTown(String DeliveryAreaTown) throws DeliveryExceptionHandler {
		// TODO Auto-generated method stub
		if (DeliveryAreaTown.isBlank() || DeliveryAreaTown.isEmpty())
			throw new DeliveryExceptionHandler("Delivery Area Town NOT specified");
		else if (DeliveryAreaTown.length() > 20)
			throw new DeliveryExceptionHandler("Delivery Area Town does not meet maximum length requirements");
		else if (DeliveryAreaTown.length() < 3)
			throw new DeliveryExceptionHandler("Delivery Area Town does not meet minimum length requirements");
		
	}
	
	public static void validateDeliveryAreaID(int DAID) throws DeliveryExceptionHandler {
		// TODO Auto-generated method stub
		if (DAID <= 0)
			throw new DeliveryExceptionHandler("Delivery Area ID can't less than or equal to 0");
		else if (DAID != (int)DAID)
			throw new DeliveryExceptionHandler("Delivery Area ID is not a integer");
		else if (DAID > 10000)
			throw new DeliveryExceptionHandler("Delivery Area ID is not exceeds more than 10000.");
		
	}
	
	
	public DeliveryArea(String DAc,String DAt,int DAid) {
		DA_country = DAc;
		DA_town = DAt;
		DA_id = DAid;
	}
	
}


