package model;

import errorHandler.ProductExceptionHandler;

public class Product {
	
	private int id;
	private String name;
	private String price;
	private String type;
	private String stauts;
	private String date;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStauts() {
		return stauts;
	}
	public void setStauts(String stauts) {
		this.stauts = stauts;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	public Product( String name, String price, String type, String stauts, String date) {
		
		try {
			validateProductName(name);
			validatePrice(Integer.parseInt(price));
			validateStock(Integer.parseInt(stauts));
			validaterTypeName(type);
		} catch (ProductExceptionHandler e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.name = name;
		this.price = price;
		this.type = type;
		this.stauts = stauts;
		this.date = date;
	}
	
	public static void validateProductName(String productName) throws ProductExceptionHandler {
		// TODO Auto-generated method stub
		if (productName.isBlank() || productName.isEmpty())
			throw new ProductExceptionHandler("productName Name NOT specified");
		else if (productName.length() > 50)
			throw new ProductExceptionHandler("Product Name does not meet minimum length requirements");
		else if (productName.length() < 5)
			throw new ProductExceptionHandler("Product Name does not meet minimum length requirements");
		
	}
	public static void validaterTypeName(String TypeName) throws ProductExceptionHandler {
		// TODO Auto-generated method stub
		if (TypeName.isBlank() || TypeName.isEmpty())
			throw new ProductExceptionHandler("product Type NOT specified");
		else if (TypeName.length() > 50)
			throw new ProductExceptionHandler("Product type Name does not meet minimum length requirements");
		else if (TypeName.length() < 5)
			throw new ProductExceptionHandler("Product type Name does not meet minimum length requirements");
		
	}
	public static void validatePrice(int price) throws ProductExceptionHandler {
		// TODO Auto-generated method stub
		if (price <= 0)
			throw new ProductExceptionHandler("Price is less than or equal to 0");
		else if (price > 1000)
			throw new ProductExceptionHandler("Price is not exceeds more than 1000.");
		
	}
	public static void validateStock(int stock) throws ProductExceptionHandler{
		// TODO Auto-generated method stub
		if (stock <= 0)
			throw new ProductExceptionHandler("stock is less than or equal to 0");
		else if (stock > 1000)
			throw new ProductExceptionHandler("stock is not exceeds more than 1000.");
	}
	
	
	
	
}
