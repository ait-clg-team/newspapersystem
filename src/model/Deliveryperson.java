package model;

import errorHandler.DeliveryExceptionHandler;

import errorHandler.ProductExceptionHandler;

public class Deliveryperson {
	public String getDP_full_name() {
		return DP_full_name;
	}

	public void setDP_full_name(String dP_full_name) {
		DP_full_name = dP_full_name;
	}

	public String getDP_phone_number() {
		return DP_phone_number;
	}

	public void setDP_phone_number(String dP_phone_number) {
		DP_phone_number = dP_phone_number;
	}

	public String getDP_address() {
		return DP_address;
	}

	public void setDP_address(String dP_address) {
		DP_address = dP_address;
	}

	public String isDP_isActive() {
		return DP_isActive;
	}

	public void setDP_isActive(String dP_isActive) {
		DP_isActive = dP_isActive;
	}

	public int getDP_subPlan() {
		return DP_subPlan;
	}

	public void setDP_subPlan(int dP_subPlan) {
		DP_subPlan = dP_subPlan;
	}

	public String getDP_created_at() {
		return DP_created_at;
	}

	public void setDP_created_at(String dP_created_at) {
		DP_created_at = dP_created_at;
	}

	String DP_full_name;
    String DP_phone_number;
	String DP_address;
	String DP_isActive;
	int DP_subPlan;
	String DP_created_at;
	
	public Deliveryperson(String DPn, String DPph, String DPadr, String DPac ,int DPsub, String DPc) {
		DP_full_name = DPn;
		DP_address = DPadr;
		DP_created_at = DPc;
		DP_phone_number = DPph;
		DP_subPlan = DPsub;
		DP_isActive = DPac;
	}
	
	public static void validateDeliverypersonName(String DeliverypersonName) throws DeliveryExceptionHandler {
		// TODO Auto-generated method stub
		if (DeliverypersonName.isBlank() || DeliverypersonName.isEmpty())
			throw new DeliveryExceptionHandler("Delivery person Name NOT specified");
		else if (DeliverypersonName.length() > 20)
			throw new DeliveryExceptionHandler("Delivery person Name does not meet maximum length requirements");
		else if (DeliverypersonName.length() < 3)
			throw new DeliveryExceptionHandler("Delivery person Name does not meet minimum length requirements");
		
	}
	
	public static void validateDeliveryAddress(String DeliveryAddress) throws DeliveryExceptionHandler {
		// TODO Auto-generated method stub
		if (DeliveryAddress.isBlank() || DeliveryAddress.isEmpty())
			throw new DeliveryExceptionHandler("Delivery Address NOT specified");
		else if (DeliveryAddress.length() > 20)
			throw new DeliveryExceptionHandler("Delivery Address does not meet maximum length requirements");
		else if (DeliveryAddress.length() < 4)
			throw new DeliveryExceptionHandler("Delivery Address does not meet minimum length requirements");	
	}
	
	public static void validateDeliveryphonenumber(String Deliveryphonenumber) throws DeliveryExceptionHandler {
		// TODO Auto-generated method stub
		if (Deliveryphonenumber.isBlank() || Deliveryphonenumber.isEmpty())
			throw new DeliveryExceptionHandler("Delivery Person Phone Number NOT specified");
		else if (Deliveryphonenumber.length() > 15)
			throw new DeliveryExceptionHandler("Delivery Person Phone Number does not meet maximum length requirements");
		else if (Deliveryphonenumber.length() < 8)
			throw new DeliveryExceptionHandler("Delivery Person Phone Number does not meet minimum length requirements");	
	}
	
	public static void validateDeliveryCreateTime(String DeliveryCreateTime) throws DeliveryExceptionHandler {
		// TODO Auto-generated method stub
		if (DeliveryCreateTime.isBlank() || DeliveryCreateTime.isEmpty())
			throw new DeliveryExceptionHandler("Delivery Create Time NOT specified");
		else if (DeliveryCreateTime.length() > 20)
			throw new DeliveryExceptionHandler("Delivery Create Time does not meet maximum length requirements");
		else if (DeliveryCreateTime.length() < 5)
			throw new DeliveryExceptionHandler("Delivery Create Time does not meet minimum length requirements");	
	}
	
	public static void validateSubPlan(int SubPlan) throws DeliveryExceptionHandler {
		// TODO Auto-generated method stub
		if (SubPlan <= 0)
			throw new DeliveryExceptionHandler("SubPlan is less than or equal to 0");
		else if (SubPlan != (int)SubPlan)
			throw new DeliveryExceptionHandler("SubPlan is not a integer");
		else if (SubPlan > 20)
			throw new DeliveryExceptionHandler("SubPlan is not exceeds more than 1000.");
		
	}
	
	public Deliveryperson() {
		// TODO Auto-generated constructor stub
	}

	public String addDPname() {
		return DP_full_name;
	}
	
	public String addDPaddress() {
		return DP_address;
	}
	
	public String addDPcreatedTime() {
		return DP_created_at;
	}
	
	public boolean addDPActive() {
		return true;
	}
	
	public String addDPPhoneNumber() {
		return DP_phone_number;
	}
	
	public int addDPsubPlan() {
		return DP_subPlan;
	}

}
