package model;

import errorHandler.CustomerExceptionHandler;

public class Customer {
	
	private int id;
	private String fullname;
	private String address;
	private String phoneNumber;
	private int age;
	private String isActive;
	private String subPlan;
	private String created_at;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getSubPlan() {
		return subPlan;
	}
	public void setSubPlan(String subPlan) {
		this.subPlan = subPlan;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	
	//Valid check
	public boolean isIdCorrect(int id)
	{
		if(id > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public boolean isNameCorrect(String name)
	{
		if(name.length()<=30 && name.length()>=2)
		{
			return true;
		}else
		{
			return false;
		}
	}
	public  boolean isAgeCorrect(int age)
	{
		if(age <=120 && age>= 16)
		{
			return true;
		}else
		{
			return false;
		}
	}
	public boolean isAddressCorrect(String address)
	{
		if(address.length() <= 50 && address.length() >= 10)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	public Customer(int id, String fullname, String address, String phoneNumber, String isActive, String subPlan,
			String created_at) {
		
		try {
			validateCustomerName(fullname);
		} catch (CustomerExceptionHandler e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.id = id;
		this.fullname = fullname;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.isActive = isActive;
		this.subPlan = subPlan;
		this.created_at = created_at;
	}

	public static  void validateCustomerName(String custName) throws CustomerExceptionHandler {
		// TODO Auto-generated method stub
		if (custName.isBlank() || custName.isEmpty())
			throw new CustomerExceptionHandler("Customer Name NOT specified");
		else if (custName.length() > 5)
			throw new CustomerExceptionHandler("Customer Name does not meet minimum length requirements");
		else if (custName.length() < 50)
			throw new CustomerExceptionHandler("Customer Name does not exceeds maximum length requirements");
	}
	public static  void validateCustomerAddress(String adress)  throws CustomerExceptionHandler  {
		// TODO Auto-generated method stub
		if (adress.isBlank() || adress.isEmpty())
			throw new CustomerExceptionHandler("adress Name NOT specified");
		else if (adress.length() > 5)
			throw new CustomerExceptionHandler("adress  does not meet minimum length requirements");
		else if (adress.length() < 50)
			throw new CustomerExceptionHandler("adress  does not exceeds maximum length requirements");
		
	}
	public static  void validateCustomerPhoneNumber(String phoneNumber) throws CustomerExceptionHandler {
		// TODO Auto-generated method stub
		if (phoneNumber.isBlank() || phoneNumber.isEmpty())
			throw new CustomerExceptionHandler("phoneNumber  NOT specified");
		else if (phoneNumber.length() > 5)
			throw new CustomerExceptionHandler("phoneNumber  does not meet minimum length requirements");
		else if (phoneNumber.length() < 50)
			throw new CustomerExceptionHandler("phoneNumber  does not exceeds maximum length requirements");
	}


}
