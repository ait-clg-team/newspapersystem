package main;

import java.sql.ResultSet;
import java.sql.SQLException;


import java.util.Scanner;

import bo.BoDeliveryArea;
import bo.BoDeliveryperson;
import bo.BoProduct;
import bo.CustomerSqlAcess;
import bo.OrderBO;
import model.Order;
import model.Product;

import model.Deliveryperson;
import model.Customer;
import model.DeliveryArea;

public class MainCommandLine {
	
	
	
	private static void listProductFuctionalityAvailable() {
		
		//Present Customer with Functionality Options
		System.out.println(" ");
		System.out.println("==================Customer===========================");
		System.out.println("Please choose ONE of the following options:");
		System.out.println("1. Create Customer");
		System.out.println("2. View ALL Customer Records");
		System.out.println("3. Delete Customer Record by ID");
		System.out.println("=============================================");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("==================Product===========================");
		System.out.println("Please choose ONE of the following options:");
		System.out.println("5. Create product");
		System.out.println("6. View ALL product Records");
		System.out.println("7. Delete product Record by ID");
		System.out.println("8. Update product Record by ID");
		System.out.println("=============================================");
		System.out.println(" ");
		System.out.println("==================Order===========================");
		System.out.println("Please choose ONE of the following options:");
		System.out.println("21. Create Order");
		System.out.println("22. View ALL Order Records");
		System.out.println("23. Delete Order Record by ID");
		System.out.println("24. Update Order Record by ID");
		System.out.println("=============================================");
		
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("================== Delivery Person=========================");
		System.out.println("Please choose ONE of the following options:");
		System.out.println("31. Add Delivery Person");
		System.out.println("32. Edit Delivery Person");
		System.out.println("33. Delete Delivery Person");

		System.out.println("===========================================================");
		System.out.println(" ");
		System.out.println("================== Delivery Area===========================");
		System.out.println("Please choose ONE of the following options:");
		System.out.println("41. Add Delivery Area ");
		System.out.println("42. View Delivery Area ");
		System.out.println("43. Edit Delivery Area ");
		System.out.println("99. Close the NewsAgent Application");
		System.out.println("===========================================================");
		
	}
	
private static boolean printProductTable(ResultSet rs) throws Exception {
		
		//Print The Contents of the Full Customer Table
		
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("Table: " + rs.getMetaData().getTableName(1));
		for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
			System.out.printf("%20s",rs.getMetaData().getColumnName(i));
		}
		System.out.println();
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
	
		System.out.println();
		while (rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("name");
			String price = rs.getString("price");
			String type = rs.getString("type");
			String status = rs.getString("status");
			String date = rs.getString("created_at");
			
			System.out.printf("%20s", id);
			System.out.printf("%20s", name);
			System.out.printf("%20s", price);
			System.out.printf("%20s", type);
			System.out.printf("%20s", status);
			System.out.printf("%20s", date);
			System.out.println();
		}// end while
		System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");
		
		return true;
		
	}









private static boolean printDeliveryTable(ResultSet rs) throws Exception {
	
	//Print The Contents of the Full Customer Table
	
	System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
	System.out.println("Table: " + rs.getMetaData().getTableName(1));
	for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
		System.out.printf("%20s",rs.getMetaData().getColumnName(i));
	}
	System.out.println();
	System.out.println("------------------------------------------------------------------------------------------------------------------------------------");

	System.out.println();
	while (rs.next()) {
		int id = rs.getInt("id");
		String DP_name = rs.getString("DP_full_name");
		String DP_phonenumber = rs.getString("DP_phone_number");
		String DP_address = rs.getString("DP_address");
		boolean DP_active = rs.getBoolean("DP_isActive");
		int DP_plan = rs.getInt("DP_subPlan");
		String DP_date = rs.getString("DP_created_at");
		String DA_country = rs.getString("DA_country");
		String DA_town = rs.getString("DA_town");
		String DA_daID = rs.getString("DA_id");

		
		
		System.out.printf("%20s", id);
		System.out.printf("%20s", DP_name);
		System.out.printf("%20s", DP_phonenumber);
		System.out.printf("%20s", DP_address);
		System.out.printf("%20s", DP_active);
		System.out.printf("%20s", DP_plan);
		System.out.printf("%20s", DP_date);
		System.out.printf("%20s", DA_country);
		System.out.printf("%20s", DA_town);
		System.out.printf("%20s", DA_daID);
		System.out.println();
	}// end while
	System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

	return true;
	
}

private static boolean printCustomerTable(ResultSet reCSet) throws SQLException {
	// TODO Auto-generated method stub
	
	
	System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
	System.out.println("Table: " + reCSet.getMetaData().getTableName(1));
	for (int i = 1; i <= reCSet.getMetaData().getColumnCount(); i++) {
		System.out.printf("%30s",reCSet.getMetaData().getColumnName(i));
	}
	System.out.println();
	while (reCSet.next()) {
		int id = reCSet.getInt("id");
		String fullname = reCSet.getString("full_name");
		String address = reCSet.getString("phone_number");
		String PhoneNumber = reCSet.getString("address");
		String isActive = reCSet.getString("isActive");
		String subPlan = reCSet.getString("subPlan");
		String created_at = reCSet.getString("created_at");
		
		System.out.printf("%30s", id);
		System.out.printf("%30s", fullname);
		System.out.printf("%30s", address);
		System.out.printf("%30s", PhoneNumber);
		System.out.printf("%30s", isActive);
		System.out.printf("%30s", subPlan);
		System.out.printf("%30s", created_at);
		System.out.println();
	}// end while
	System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

	return true;
	
}


private static boolean printOrderTable(ResultSet reCSet) throws SQLException {
	// TODO Auto-generated method stub
	
	
	System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
	System.out.println("Table: " + reCSet.getMetaData().getTableName(1));
	for (int i = 1; i <= reCSet.getMetaData().getColumnCount(); i++) {
		System.out.printf("%30s",reCSet.getMetaData().getColumnName(i));
	}
	System.out.println();
	while (reCSet.next()) {
		int id = reCSet.getInt("id");
		String status = reCSet.getString("status");
		String order_date = reCSet.getString("order_date");
		String update_date = reCSet.getString("update_date");
		String billing_date = reCSet.getString("billing_date");
		String created_at = reCSet.getString("created_at");
		String customer_id = reCSet.getString("customer_id");
		String proudct_id = reCSet.getString("proudct_id");
		String area_id = reCSet.getString("area_id");
		
		System.out.printf("%30s", id);
		System.out.printf("%30s", status);
		System.out.printf("%30s", order_date);
		System.out.printf("%30s", update_date);
		System.out.printf("%30s", billing_date);
		System.out.printf("%30s", created_at);
		System.out.printf("%30s", customer_id);
		System.out.printf("%30s", proudct_id);
		System.out.printf("%30s", area_id);
		System.out.println();
	}// end while
	System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

	return true;
	
}



	
	
public static void main(String[] args) {
		
		//Create the Database Object
		
		try {
//			Scanner sc= new Scanner(System.in); //System.in is a standard input stream  
			
			BoProduct dao = new BoProduct();
			dao.initiate_db_conn();
			
			OrderBO orderdao = new OrderBO();
			orderdao.initiate_db_conn();
			
			CustomerSqlAcess custBo = new CustomerSqlAcess();
			custBo.initiate_db_conn();
			
			BoDeliveryperson DPdao = new BoDeliveryperson();
			DPdao.initiate_db_conn();
		
			// Configure System for Running
			Scanner keyboard = new Scanner(System.in); 
			String functionNumber = "-99";
			boolean keepAppOpen = true;
		
			while (keepAppOpen == true) {
			
				//Present list of functionality and get selection
				listProductFuctionalityAvailable();
				functionNumber = keyboard.next();
		
				switch (functionNumber) {
				
				case "1":
					//Get Customer Details from the User
					keyboard.nextLine();  
					System.out.printf("Enter Customer Full Name: \n");
					String fullname = keyboard.nextLine();
					System.out.println(" name - "+ fullname);
					System.out.printf("Enter phone: \n");
					String phone = keyboard.next();
					System.out.printf("Enter address : \n");
					keyboard.nextLine();
					String address = keyboard.nextLine();
					System.out.printf("Enter is active Y/N ): \n");
					String isactive = keyboard.next();
					System.out.printf("Enter is plan id ): \n");
					String plan_id = keyboard.next();
					System.out.printf("Enter date (dd/mm/yy): \n");
					String date = keyboard.next();
					
					Customer customerObj = new Customer(0, fullname, address, phone, isactive, plan_id, date );
					
					//Insert Customer Details into the database
					boolean insertCustomerDetailsAccount =  custBo.insertCustomerDetailsAccount(customerObj);
					if (insertCustomerDetailsAccount == true)
						System.out.println("Customer Details Saved");
					else 
						System.out.println("ERROR: Customer Details NOT Saved");
			
					

					
					
				//	System.out.println("create customer");
					break;
					
				case "2": 
					//Retrieve ALL Customer Records
					ResultSet rSet = custBo.retrieveAllCustomerAccounts();
					if (rSet == null) {
						System.out.println("No Records Found");
						break;
					}
					else {
						boolean tablePrinted = printCustomerTable(rSet);
						if (tablePrinted == true)
							rSet.close();
					}
					break;
		
				case "5":
					keyboard.nextLine();  
					//Get Customer Details from the User
					System.out.printf("Enter product Name: \n");
					String name = keyboard.nextLine();  
					System.out.printf("Enter price: \n");
					String price = keyboard.next();
					System.out.printf("Enter product type(News/e-News): \n");
					String type = keyboard.next();
					System.out.printf("Enter product stauts(): \n");
					String stauts = keyboard.next();
					System.out.printf("Enter date (dd/mm/yy): \n");
					String dateCustomer = keyboard.next();
				
					Product custObj = new Product(name,price,type,stauts,dateCustomer);
				
					//Insert Customer Details into the database
					boolean insertResult = dao.insertProductDetails(custObj);
					if (insertResult == true)
						System.out.println("Product Details Saved");
					else 
						System.out.println("ERROR: Product Details NOT Saved");
					break;
					
				case "6": 
					//Retrieve ALL Customer Records
					ResultSet rSet1 = dao.retrieveAllProductAccounts();
					if (rSet1 == null) {
						System.out.println("No Records Found");
						break;
					}
					else {
						boolean tablePrinted = printProductTable(rSet1);
						if (tablePrinted == true)
							rSet1.close();
					}
					break;
				case "7":
					//Delete Customer Record by ID
					System.out.println("Enter Product Id to be deleted or -99 to Clear all Rows");
					String deleteProdId = keyboard.next();
					boolean deleteResult = dao.deleteProductById(Integer.parseInt(deleteProdId));
					if ((deleteResult == true) && (deleteProdId.equals("-99")))
						System.out.println("Product Table Emptied");
					else if (deleteResult == true)
						System.out.println("Product Deleted");
					else 
						System.out.println("ERROR: Product Details NOT Deleted or Do Not Exist");
					break;
					
				case "8":
					
					System.out.printf("Enter Product Id: \n");
					int proId = keyboard.nextInt();
					keyboard.nextLine();  
					//Get Customer Details from the User
					System.out.printf("Enter product Name: \n");
					String uName = keyboard.nextLine();  
					Product ProductObj = new Product(uName,"","","","");
				
					//Insert Customer Details into the database
					boolean updateResult = dao.updateProductById(ProductObj,proId);
					if (updateResult == true)
						System.out.println("Product Details Updated");
					else 
						System.out.println("ERROR: Product Details NOT Updated");
					break;
					
				case "21":
					//Get Customer Details from the User
					System.out.printf("Enter Customer Id: \n");
					int customer_id = keyboard.nextInt();
					System.out.printf("Enter Product Id: \n");
					int product_id = keyboard.nextInt();
					System.out.printf("Enter area id: \n");
					int area_id = keyboard.nextInt();
					System.out.printf("Enter order stauts (Pending/Rejected/Delivered): \n");
					String Ostauts = keyboard.next();
					System.out.printf("Enter Order date (dd/mm/yy): \n");
					String Order_date = keyboard.next();
					System.out.printf("Enter update date (dd/mm/yy): \n");
					String Update_date = keyboard.next();
					System.out.printf("Enter billing date (dd/mm/yy): \n");
					String billing_date = keyboard.next();
					System.out.printf("Enter create date (dd/mm/yy): \n");
					String create_date = keyboard.next();
				
					Order orderObj = new Order(customer_id,product_id,area_id,Ostauts,Order_date,Update_date,billing_date,create_date);
				
					//Insert Customer Details into the database
					boolean insertOrderResult =  orderdao.insertOrderDetails(orderObj);
					if (insertOrderResult == true)
						System.out.println("Order Details Saved");
					else 
						System.out.println("ERROR: Order Details NOT Saved");
					break;
				case "22": 
					//Retrieve ALL Customer Records
					ResultSet rOSet = orderdao.retrieveAllOrder();
					if (rOSet == null) {
						System.out.println("No Records Found");
						break;
					}
					else {
						boolean tablePrinted = printOrderTable(rOSet);
						if (tablePrinted == true)
							rOSet.close();
					}
					break;
				case "24":
					
					System.out.printf("Enter Order Id: \n");
					int orderId = keyboard.nextInt();
					keyboard.nextLine();  
					//Get Order Details from the User
					System.out.printf("Enter order stauts (Pending/Rejected/Delivered): \n");
					String ustatus = keyboard.nextLine();  
					
					Order orderUpdateObj = new Order(0,0,0,ustatus,"","","","");
				
					//Insert Customer Details into the database
					boolean updateOrderResult = orderdao.updateOrderDetailsById(orderUpdateObj,orderId);
					if (updateOrderResult == true)
						System.out.println("Order Details Updated");
					else 
						System.out.println("ERROR: Order Details NOT Updated");
					break;
				case "23":
					//Delete Customer Record by ID
					System.out.println("Enter Product Id to be deleted or -99 to Clear all Rows");
					String deleteOrderId = keyboard.next();
					boolean deleteOrderResult = orderdao.deleteOrderById(Integer.parseInt(deleteOrderId));
					if ((deleteOrderResult == true) && (deleteOrderId.equals("-99")))
						System.out.println("Order Table Emptied");
					else if (deleteOrderResult == true)
						System.out.println("Order Deleted");
					else 
						System.out.println("ERROR: Order Details NOT Deleted or Do Not Exist");
					break;
			
					
				case "31":
					keyboard.nextLine();
					System.out.printf("Enter Delivery Person Nmae: \n");
					String DP_name = keyboard.nextLine();
					System.out.printf("Enter Delivery Person Phone Number: \n");
					String DP_phonenumber = keyboard.next();
					keyboard.nextLine();
					System.out.printf("Enter Delivery Person Address: \n");
			
					String DP_address = keyboard.nextLine();
					System.out.printf("Enter Delivery Person Actice: Y/N \n");
					String DP_active = keyboard.next();
//					System.out.printf("Enter Delivery Person SubPlan ID: \n");
//					int DP_plan = keyboard.nextInt();
					System.out.printf("Enter Delivery Person Created Time (dd/mm/yy): \n");
					String DP_date = keyboard.next();

					Deliveryperson DPObj = new Deliveryperson(DP_name,DP_phonenumber,DP_address,DP_active,0,DP_date);
					
					//InsertDeliveryperson Details into the database
					boolean insertDeliverypersonResult =  DPdao.insertDeliverypersonDetails(DPObj);
					if (insertDeliverypersonResult == true)
						System.out.println("Deliveryperson Details Saved");
					else 
						System.out.println("ERROR: Deq31liveryperson Details NOT Saved");
					
					break;
					
					case "32":
					
					System.out.printf("Enter New Delivery Person Name: \n");
					String DP_name1 = keyboard.next();
					System.out.printf("Enter New Delivery Person Phone Number: \n");
					String DP_address1 = keyboard.next();
					System.out.printf("Enter New Delivery Person Address: \n");
					String DP_phonenumber1 = keyboard.next();
					System.out.printf("Enter New Delivery Person Actice(Yes/No): \n");
					String DP_active1 = keyboard.next();
					System.out.printf("Enter New Delivery Person SubPlan: \n");
					int DP_plan1 = keyboard.nextInt();
					System.out.printf("Enter New Delivery Person Created Time: \n");
					String DP_date2 = keyboard.next();

					Deliveryperson DPObj1 = new Deliveryperson(DP_name1,DP_address1,DP_phonenumber1,DP_active1,DP_plan1,DP_date2);
					
					//InsertDeliveryperson Details into the database
					boolean updateDeliverypersonResult =  DPdao.insertDeliverypersonDetails(DPObj1);
					if (updateDeliverypersonResult == true)
						System.out.println("Deliveryperson Details Updated");
					else 
						System.out.println("ERROR: Deliveryperson Details NOT Updated");
					
					break;
					
					case "33":
						
					System.out.printf("Enter delete Delivery Person ID: \n");
					int DP_id2 = keyboard.nextInt();
					
					
					//InsertDeliveryperson Details into the database
					boolean deleteDeliverypersonResult =  DPdao.deleteDeliverypersonDetails(DP_id2);
					if (deleteDeliverypersonResult == true)
						System.out.println("Deliveryperson Details Deleted");
					else 
						System.out.println("ERROR: Deliveryperson Details NOT Deleted");
					
					break;
					
				case "41":
					
					System.out.printf("Enter Delivery Area ID: \n");
					int DA_ID = keyboard.nextInt();
					System.out.printf("Enter Delivery Area Country: \n");
					String DA_country = keyboard.next();
					System.out.printf("Enter Delivery Area Town: \n");
					String DA_town = keyboard.next();
					
					DeliveryArea DAObj = new DeliveryArea(DA_town,DA_country,DA_ID);
					
					//InsertDeliveryperson Details into the database
					boolean insertDeliveryAreaResult =  DPdao.insertDeliveryAreaDetails(DAObj);
					if (insertDeliveryAreaResult == true)
						System.out.println("Delivery Area Details Saved");
					else 
						System.out.println("ERROR: Delivery Area Details NOT Saved");

				case "42":
					System.out.println("These are all Delivery Area: \n");
					//System.out.println(ViewDeliveryArea());
					
				case "43":
					
					System.out.printf("Enter Delivery Area ID: \n");
					int DA_ID1 = keyboard.nextInt();
					System.out.printf("Enter Delivery Area Country: \n");
					String DA_country1 = keyboard.next();
					System.out.printf("Enter Delivery Area Town: \n");
					String DA_town1 = keyboard.next();
					
					DeliveryArea DAObj1 = new DeliveryArea(DA_town1,DA_country1,DA_ID1);
					
					//InsertDeliveryperson Details into the database
					boolean updateDeliveryAreaResult =  DPdao.insertDeliveryAreaDetails(DAObj1);
					if (updateDeliveryAreaResult == true)
						System.out.println("Delivery Area Details Saved");
					else 
						System.out.println("ERROR: Delivery Area Details NOT Saved");
					
				case "99":
					keepAppOpen = false;
					System.out.println("Closing the Application");
					break;
			
				default:
					System.out.println("No Valid Function Selected");
					break;
				} // end switch
		
			}// end while
		
			//Tidy up Resources
			keyboard.close();
		
		}
	
		catch(Exception e) {
			System.out.println("PROGRAM TERMINATED - ERROR MESSAGE:" + e.getMessage());
		} // end try-catch
		

	} // end main


	
	
}