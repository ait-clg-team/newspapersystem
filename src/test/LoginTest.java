package test;

import junit.framework.TestCase;
import model.Login;


public class LoginTest extends TestCase {
	
	// test number: 01
	// input: h
	// expected output: exception
	// description: test if the email  function returns the correct
	// 				exception when the email is less than 2 characters
	public void testLogin001() {
		try {
			Login login = new Login();
			login.isUNCorrect("h");
		} catch (Exception e) {
			assertEquals("username invalid, less than 2 characters", e.getMessage());
		}
	}

	// test number: 02
	// input: aaaaaaaaaaaaaaaaaaaaaa
	// expected output: exception
	// description: test if the email check function returns the correct
	// 				exception when the email is more than 20 characters
	public void testLogin002() {
		
		Login loginObj = new Login();
				
		try {
			Login login = new Login();
			login.isUNCorrect("aaaaaaaaaaaaaaaaaaaaaa");
		} catch (Exception e) {
			assertEquals("username invalid, more than 20 characters", e.getMessage());
		}
	}

	// test number: 03
	// input: aa
	// expected output: exception
	// description: test if the password check function returns the correct
	// 				exception when the username is less than 2 characters
	public void testLogin003() {
		try {
			Login.isPassCorrect("aa");
		} catch (Exception e) {
			assertEquals("password invalid, less than 2 characters", e.getMessage());
		}
	}

	// test number: 04
	// input: aaaaaaaaaaaaaaaaaaaaaa
	// expected output: exception
	// description: test if the password check function returns the correct
	// 				exception when the username is more than 20 characters
	public void testLogin004() {
		try {
			Login.isPassCorrect("aaaaaaaaaaaaaaaaaaaaaa");
		} catch (Exception e) {
			assertEquals("password invalid, more than 20 characters", e.getMessage());
		}
	}

}
