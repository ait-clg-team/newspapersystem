package test;

import java.sql.ResultSet;

import bo.BoProduct;
import bo.CustomerSqlAcess;
import errorHandler.CustomerExceptionHandler;
import junit.framework.TestCase;
import model.Customer;
import model.Product;


/**
 * @author Mukendi David Katala
 *
 */

public class CustomerTest extends TestCase {
	
	
	    
		
		//Test #: 2
		//Test Objective: To catch an invalid customer Full name (low numbers of character than expected)
		//Inputs: custName = "J"
		//Expected Output: Exception Message: "Customer Name does not meet minimum length requirements"

		public void testValidateCustomerName001() {
				
			try {
					
				//Customer customer = new Customer(0, "J", "Athlone", "087-123123123", "yes", " ", "25/01/2022");
				//Call method under test
				Customer.validateCustomerName("hs");
				fail("Exception expected");
			}
			catch (CustomerExceptionHandler e) {
				System.out.println(e.getMessage());
				assertEquals("Customer Name does not exceeds maximum length requirements", e.getMessage());	
			}
		}
		
		//Test #: 2
				//Test Objective: To catch an invalid customer Full name (low numbers of character than expected)
				//Inputs: custName = "J"
				//Expected Output: Exception Message: "Customer Name does not meet minimum length requirements"

				public void testValidateCustomerName002() {
						
					try {
							
						//Customer customer = new Customer(0, "J", "Athlone", "087-123123123", "yes", " ", "25/01/2022");
						//Call method under test
						Customer.validateCustomerName(" ");
						fail("Exception expected");
					}
					catch (CustomerExceptionHandler e) {
						System.out.println(e.getMessage());
						assertEquals("Customer Name NOT specified", e.getMessage());	
					}
				}
		
		//Test #: 3
		//Test Objective: To catch an invalid customer address 
		//Inputs: custName = "J"
		//Expected Output: Exception Message: "Customer Address does not meet minimum length requirements"			

		
		public void testValidateAddress003() {
			
		try {	//Call method under test
			// customer = new Customer(0, "Jack Daniels", "J", "087-123123123", "yes", " ", "25/01/2022");
			Customer.validateCustomerAddress("Jsdffgdfg");
			fail("Exception expected");
		}
		catch (CustomerExceptionHandler e) {
			System.out.println(e.getMessage());
			assertEquals("adress  does not meet minimum length requirements", e.getMessage());	
		}
		
		}
		
		//Test #: 4
		//Test Objective: To catch an invalid customer phone number 
		//Inputs: custName = "J"
		//Expected Output: Exception Message: "Customer Phone number does not meet minimum length requirements"			

		
		public void testValidatePhoneNumber004() {
		try {	
		//Customer customer = new Customer(0, "Jack Daniels", "Athlone", "087-123123123", "yes", " ", "25/01/2022");
			Customer.validateCustomerPhoneNumber("J");
			fail("Exception expected");
		}
		catch (CustomerExceptionHandler e){
			System.out.println(e.getMessage());
			assertEquals("phoneNumber  does not exceeds maximum length requirements", e.getMessage());	
		}
		
		} 
	
		//Test #: 4
		//Test Objective: To catch an invalid customer phone number 
		//Inputs: custName = "J"
		//Expected Output: Exception Message: "Customer Phone number does not meet minimum length requirements"			

		
		public void testValidatePhoneNumber005() {
		try {	
		//Customer customer = new Customer(0, "Jack Daniels", "Athlone", "087-123123123", "yes", " ", "25/01/2022");
			Customer.validateCustomerPhoneNumber(" ");
			fail("Exception expected");
		}
		catch (CustomerExceptionHandler e){
			System.out.println(e.getMessage());
			assertEquals("phoneNumber  NOT specified", e.getMessage());	
		}
		
		} 
		
		public void testaddcustomertTest001() throws Exception {
			//Call method under test
			CustomerSqlAcess testObj = new CustomerSqlAcess();
//			testObj.initiate_db_conn();
			Customer pe = new Customer(1, "bhsdghf shdgfhds", "asfsdfgdfgfdgdf", "123456789", "yes", "1","12/12/12");
			boolean res = testObj.insertCustomerDetailsAccount(pe);
			System.out.println(res);
			assertEquals(false, res);	
			
		}
		
		
		
		
		public void testdeletecustomerTest0012() throws Exception {
			//Call method under test
			CustomerSqlAcess testObj = new CustomerSqlAcess();
			testObj.initiate_db_conn();
			Customer pe = new Customer(1, "bhsdghf shdgfhds", "asfsdfgdfgfdgdf", "123456789", "yes", "1","12/12/12");
			
			boolean res = testObj.deleteCustomerById(2);
			System.out.println(res);
			assertEquals(true, res);	
			
		}
		
		public void testDisplayproductTest0012() throws Exception {
			//Call method under test
			CustomerSqlAcess testObj = new CustomerSqlAcess();
			testObj.initiate_db_conn();
			Customer pe = new Customer(1, "bhsdghf shdgfhds", "asfsdfgdfgfdgdf", "123456789", "yes", "1","12/12/12");
			
			ResultSet res = testObj.retrieveAllCustomerAccounts();
			ResultSet resultSet = res;
			System.out.println(res);
			assertEquals(resultSet, res);	
			
		}
		
		
		
		
		
		
		

}
