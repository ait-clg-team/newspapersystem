package test;


import bo.BoDeliveryperson;
import errorHandler.DeliveryExceptionHandler;
import junit.framework.TestCase;
import model.Deliveryperson;
import java.io.IOException;
import main.MainCommandLine;


/*
 * 
 *  Jason Test
 * 
 * */


public class DeliverypersonTest  extends TestCase  {
	
	
	/**
	 * Test Number:1

	 * Test Objective:DeliveryPerson()

	 * Input(s):  Niko, 0870956375, athlone, true, 4, 2022-01-20

	 * Expected Output(s):Niko, 0870956375, athlone, true, 4, 2022-01-20

	 */
	
	public void testaddDeliveryPersont001() {
		//Call method under test
		BoDeliveryperson testObj = new BoDeliveryperson();
		Deliveryperson dp = new Deliveryperson("Niko", "0870956375", "athlone", "yes", 4, "2022-01-20");
		boolean res = testObj.insertDeliverypersonDetails(dp);
		assertEquals(true, res);	
		
	}

/**
 * Test Number:2

 * Test Objective:addDeliveryPersonName()

 * Input(s):  Ni

 * Expected Output(s):Delivery Person Name does not meet minimum length requirements

 */
	
	public void testDeliverypersonValidateName002() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliverypersonName("Ni");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				assertEquals("Delivery person Name does not meet minimum length requirements", e.getMessage());	
			}
		}
	
	
	public void testDeliverypersonValidateName0013() {
		
		try {
			//Call method under test
			Deliveryperson.validateDeliverypersonName("Nsdfsdfi");
			fail("Exception expected");
		}
		catch (DeliveryExceptionHandler e) {
			assertEquals("Delivery person Name does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	
	/**
	 * Test Number:3

	 * Test Objective:addDeliveryPersonName()

	 * Input(s):  null

	 * Expected Output(s):Delivery Person Name NOT specified

	 */
	
	public void testDeliverypersonValidateName003() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliverypersonName("");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				System.out.println(e+"");
				assertEquals("Delivery person Name NOT specified", e.getMessage());	
			}
		}
	
	/**
	 * Test Number:4

	 * Test Objective:addDeliveryPersonName()

	 * Input(s): zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz

	 * Expected Output(s):Delivery Person Name NOT specified

	 */
	
	public void testDeliverypersonValidateName004() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliverypersonName("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				System.out.println(e+"");
				assertEquals("Delivery person Name does not meet maximum length requirements", e.getMessage());	
			}
		}
	
	
	
	/**
	 * Test Number:4

	 * Test Objective:addDeliveryPersonaddress()

	 * Input(s):  ath

	 * Expected Output(s):Delivery Person Address does not meet minimum length requirements

	 */
	
	public void testDeliverypersonValidateaddress005() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliveryAddress("ath");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				assertEquals("Delivery Address does not meet minimum length requirements", e.getMessage());	
			}
		}
	
	/**
	 * Test Number:5

	 * Test Objective:addDeliveryPersonaddress()

	 * Input(s):  ath

	 * Expected Output(s):Delivery Person Address does not meet minimum length requirements

	 */
	
	public void testDeliverypersonValidateaddress006() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliveryAddress("");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				assertEquals("Delivery Address does not meet minimum length requirements", e.getMessage());	
			}
		}
	
	/**
	 * Test Number:6

	 * Test Objective:addDeliveryPersonaddress()

	 * Input(s):  ath

	 * Expected Output(s):Delivery Person Address does not meet minimum length requirements

	 */
	
	public void testDeliverypersonValidateaddress007() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliveryAddress("zzzzzzzzzzzzzzzzzzzzzzzzzzzz");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				assertEquals("Delivery Address does not meet maximum length requirements", e.getMessage());	
			}
		}
	
	/**
	 * Test Number:7

	 * Test Objective:addDeliveryPersonaddress()

	 * Input(s):  ath

	 * Expected Output(s):Delivery Person Address does not meet minimum length requirements

	 */
	
	public void testDeliverypersonValidatephonenumber008() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliveryphonenumber("123465215425645124376582542564");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				assertEquals("Delivery Person Phone Number NOT specified", e.getMessage());	
			}
		}
	
	/**
	 * Test Number:9

	 * Test Objective:addDeliveryPersonaddress()

	 * Input(s):  ath

	 * Expected Output(s):Delivery Person Address does not meet minimum length requirements

	 */
	
	public void testDeliverypersonValidatephonenumber009() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliveryphonenumber("");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				assertEquals("Delivery Person Phone Number NOT specified", e.getMessage());	
			}
		}
	
	/**
	 * Test Number:10

	 * Test Objective:addDeliveryPersonaddress()

	 * Input(s):  ath

	 * Expected Output(s):Delivery Person Address does not meet minimum length requirements

	 */
	
	public void testDeliverypersonValidatephonenumber0010() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliveryphonenumber("1234");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				assertEquals("Delivery Person Phone Number does not meet minimum length requirements", e.getMessage());	
			}
		}
	
	/**
	 * Test Number:11

	 * Test Objective:addDeliveryPersonaddress()

	 * Input(s):  ath

	 * Expected Output(s):Delivery Person Address does not meet minimum length requirements

	 */
	
	public void testDeliverypersonValidateCreateTime0011() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliveryCreateTime("");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				assertEquals("Delivery Create Time NOT specified", e.getMessage());	
			}
		}
	
	/**
	 * Test Number:12

	 * Test Objective:addDeliveryPersonaddress()

	 * Input(s):  ath

	 * Expected Output(s):Delivery Person Address does not meet minimum length requirements

	 */
	
	public void testDeliverypersonValidateCreateTime0012() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliveryCreateTime("20132565676465851659364584");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				assertEquals("Delivery Create Time does not meet maximum length requirements", e.getMessage());	
			}
		}
	
	
	/**
	 * Test Number:13

	 * Test Objective:addDeliveryPersonaddress()

	 * Input(s):  ath

	 * Expected Output(s):Delivery Person Address does not meet minimum length requirements

	 */
	
	public void testDeliverypersonValidateCreateTime0013() {
		
			try {
				//Call method under test
				Deliveryperson.validateDeliveryCreateTime("123");
				fail("Exception expected");
			}
			catch (DeliveryExceptionHandler e) {
				assertEquals("Delivery Create Time does not meet minimum length requirements", e.getMessage());	
			}
		}
	
	
	/**
	 * Test Number:14

	 * Test Objective:addDeliveryPersonSubplan()

	 * Input(s):  0

	 * Expected Output(s):Sub Plan is less than or equal to 0

	 */
	
	
	public void testSubPlanValidate0014() {
		
		try {
			//Call method under test
			Deliveryperson.validateSubPlan(0);
			fail("Exception expected");
		}
		catch (DeliveryExceptionHandler e) {
			assertEquals("SubPlan is less than or equal to 0", e.getMessage());	
		}
	}
		

			
			/**
			 * Test Number:15

			 * Test Objective:addDeliveryPersonSubplan()

			 * Input(s):  0

			 * Expected Output(s):Sub Plan is less than or equal to 0

			 */
			
			
			public void testSubPlanValidate0015() {
				
				try {
					//Call method under test
					Deliveryperson.validateSubPlan(5000);
					fail("Exception expected");
				}
				catch (DeliveryExceptionHandler e) {
					assertEquals("SubPlan is not exceeds more than 1000.", e.getMessage());	
				}

	}
}
