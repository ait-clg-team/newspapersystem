package test;

import junit.framework.TestCase;
import bo.BoDeliveryArea;
import bo.BoDeliveryperson;
import errorHandler.DeliveryExceptionHandler;
import model.DeliveryArea;
import model.Deliveryperson;

import java.io.IOException;
import main.MainCommandLine;

public class DeliveryAreaTest extends TestCase{

	/**
	 * Test Number :  1
	 * Test Objective:  addDeliveryArea()
	 * Input(s): Ireland   Athlone  001
	 * Output(s): Ireland   Athlone  001
	 */

	
	
	public void DeliveryAreat001() {
		//Call method under test
		BoDeliveryArea testObj = new BoDeliveryArea();
		DeliveryArea da = new DeliveryArea("Ireland","Athlone",101);
		boolean res = testObj.insertDeliveryAreaDetails(da);
		fail("Exception expected");
		assertEquals(true, res);	
		
	}
	
	
	
	/**
	 * Test Number :  2
	 * Test Objective:  addDeliveryCountry()
	 * Input(s): Ir
	 * Output(s): Delivery Area Country does not meet minimum length requirements
	 */
	
	public void testDeliveryAreaValidateCountry002() {
		
		try {
			//Call method under test
			DeliveryArea.validateDeliveryAreaCountry("Ir");
			fail("Exception expected");
		}
		catch (DeliveryExceptionHandler e) {
			assertEquals("Delivery Area Country does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	/**
	 * Test Number :  3
	 * Test Objective:  addDeliveryCountry()
	 * Input(s): Ir
	 * Output(s): Delivery Area Country does not meet minimum length requirements
	 */
	
	public void testDeliveryAreaValidateCountry003() {
		
		try {
			//Call method under test
			DeliveryArea.validateDeliveryAreaCountry("");
			fail("Exception expected");
		}
		catch (DeliveryExceptionHandler e) {
			assertEquals("Delivery Area Country NOT specified", e.getMessage());	
		}
	}
	
	/**
	 * Test Number :  4
	 * Test Objective:  addDeliveryCountry()
	 * Input(s): Ir
	 * Output(s): Delivery Area Country does not meet minimum length requirements
	 */
	
	public void testDeliveryAreaValidateCountry004() {
		
		try {
			//Call method under test
			DeliveryArea.validateDeliveryAreaCountry("zxcasdasdasdasdassdasdasdasdas");
			fail("Exception expected");
		}
		catch (DeliveryExceptionHandler e) {
			assertEquals("Delivery Area Country does not meet maximum length requirements", e.getMessage());	
		}
	}
	
	
	
	
	/**
	 * Test Number :  5
	 * Test Objective:  addDeliveryTown()
	 * Input(s): At
	 * Output(s): Delivery Area Town does not meet minimum length requirements
	 */
	
	public void testDeliveryAreaValidateTown005() {
		
		try {
			//Call method under test
			DeliveryArea.validateDeliveryAreaTown("At");
			fail("Exception expected");
		}
		catch (DeliveryExceptionHandler e) {
			assertEquals("Delivery Area Town does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	
	/**
	 * Test Number :  6
	 * Test Objective:  addDeliveryTown()
	 * Input(s): At
	 * Output(s): Delivery Area Town does not meet minimum length requirements
	 */
	
	public void testDeliveryAreaValidateTown006() {
		
		try {
			//Call method under test
			DeliveryArea.validateDeliveryAreaTown("");
			fail("Exception expected");
		}
		catch (DeliveryExceptionHandler e) {
			assertEquals("Delivery Area Town NOT specified", e.getMessage());	
		}
	}
	
	/**
	 * Test Number :  7
	 * Test Objective:  addDeliveryTown()
	 * Input(s): At
	 * Output(s): Delivery Area Town does not meet minimum length requirements
	 */
	
	public void testDeliveryAreaValidateTown007() {
		
		try {
			//Call method under test
			DeliveryArea.validateDeliveryAreaTown("sdgsdygwdygysfgushfushfuhsufhushfusf");
			fail("Exception expected");
		}
		catch (DeliveryExceptionHandler e) {
			assertEquals("Delivery Area Town does not meet maximum length requirements", e.getMessage());	
		}
	}
	
	/**
	 * Test Number :  8
	 * Test Objective:  addDeliveryTown()
	 * Input(s): 0
	 * Output(s): Delivery Area ID can't less than or equal to 0
	 */
	
	public void testDeliveryArewIDValidate008() {
		
		try {
			//Call method under test
			DeliveryArea.validateDeliveryAreaID(0);
			fail("Exception expected");
		}
		catch (DeliveryExceptionHandler e) {
			assertEquals("Delivery Area ID can't less than or equal to 0", e.getMessage());	
		}
	}
	
	/**
	 * Test Number :  9
	 * Test Objective:  addDeliveryTown()
	 * Input(s): 0
	 * Output(s): Delivery Area ID can't less than or equal to 0
	 */
	
	public void testDeliveryArewIDValidate009() {
		
		try {
			//Call method under test
			DeliveryArea.validateDeliveryAreaID(10000000);
			fail("Exception expected");
		}
		catch (DeliveryExceptionHandler e) {
			assertEquals("Delivery Area ID is not exceeds more than 10000.", e.getMessage());	
		}
	}
}
