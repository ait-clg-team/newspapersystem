package test;

import java.sql.ResultSet;

import bo.BoProduct;
import bo.OrderBO;
import errorHandler.OrderExceptionHandler;
import errorHandler.ProductExceptionHandler;
import junit.framework.TestCase;
import model.Order;
import model.Product;

public class OrderTest extends TestCase {
	
	
	
	//Test #: 1
	//Test Objective: check customer id
	//Inputs: customer id = " "
	//Expected Output: Exception Message: "customer id is less than or equal to zero"
	public void testValidateCustomerId001() {
		
		try {
			//Call method under test
			Order.validateCustomerID(-1);
			fail("Exception expected");
		}
		catch (OrderExceptionHandler e) {
			System.out.println(e+"");
			assertEquals("customer id is less than or equal to zero", e.getMessage());	
		}
	}
	

	
		//Test #: 2
		//Test Objective: check product id
		//Inputs: customer id = "0"
		//Expected Output: Exception Message: "customer id is less than or equal to zero"
		public void testValidateProductId002() {
			
			try {
				//Call method under test
				Order.validateProductID(0);
				fail("Exception expected");
			}
			catch (OrderExceptionHandler e) {
				System.out.println(e+"");
				assertEquals("product id is less than or equal to zero", e.getMessage());	
			}
		}
		
		//Test #: 3
		//Test Objective: check area id
		//Inputs: customer id = "-1"
		//Expected Output: Exception Message: "customer id is less than or equal to zero"
		public void testValidateAreaId002() {
			
			try {
				//Call method under test
				Order.validateAreaID(-1);
				fail("Exception expected");
			}
			catch (OrderExceptionHandler e) {
				System.out.println(e+"");
				assertEquals("area id is less than or equal to zero", e.getMessage());	
			}
		}
		
		public void testOrderTest001() throws Exception {
			//Call method under test
			OrderBO testObj = new OrderBO();
//			testObj.initiate_db_conn();
			Order pe = new Order(1, 1, 1, "","","", "", "");
			boolean res = testObj.insertOrderDetails(pe);
			System.out.println(res);
			assertEquals(false, res);	
			
		}
		
		public void testOrderTest002() throws Exception {
			//Call method under test
			OrderBO testObj = new OrderBO();
//			testObj.initiate_db_conn();
			Order pe = new Order(1, 1, 1, "","","", "", "");
//			boolean res = testObj.insertOrderDetails(pe);
//			System.out.println(res);
			assertEquals(pe.getArea_id(), 1);
			assertEquals(pe.getCustomer_id(), 1);
			assertEquals(pe.getProduct_id(), 1);
			assertEquals(pe.getBilling_date(),"");
			assertEquals(pe.getCreate_at(), "");
			assertEquals(pe.getUpdate_date(), "");
			
		}
		
		public void testDisplayOrderTest0012() throws Exception {
			//Call method under test
			OrderBO testObj = new OrderBO();
			testObj.initiate_db_conn();
			ResultSet resultSet = null;
			Order pe = new Order(1, 1, 1, "","","", "", "");;
			ResultSet res = testObj.retrieveAllOrder();
			System.out.println(res);
			resultSet = res;
			assertEquals(resultSet, res);	
			
		}
		
		public void testinsertOrderTest0012() throws Exception {
			//Call method under test
			OrderBO testObj = new OrderBO();
			testObj.initiate_db_conn();

			Order pe = new Order(1, 1, 1, "12/12/12","12/12/12","12/12/12", "12/12/12", "12/12/12");
			boolean res = testObj.insertOrderDetails(pe);
			System.out.println(res);
			assertEquals(true, res);	
			
			
			
		}
		
		public void testUpadteOrderTest0012() throws Exception {
			//Call method under test
			OrderBO testObj = new OrderBO();
			testObj.initiate_db_conn();
			ResultSet resultSet = null;
			Order pe = new Order(1, 1, 1, "12/12/12","12/12/12","12/12/12", "12/12/12", "12/12/12");
			boolean res = testObj.updateOrderDetailsById(pe,1);
			System.out.println(res);
			assertEquals(true, res);	
			
		}
		
		public void testdeleteproductTest0011() throws Exception {
			//Call method under test
			OrderBO testObj = new OrderBO();
			//testObj.initiate_db_conn();
			Order pe = new Order(1, 1, 1, "12/12/12","12/12/12","12/12/12", "12/12/12", "12/12/12");
			boolean res = testObj.deleteOrderById(2);
			System.out.println(res);
			assertEquals(false, res);	
			
		}
		

}
