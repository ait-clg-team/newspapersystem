/**
 * 
 */
package test;

import java.io.IOException;
import java.sql.ResultSet;

import bo.BoProduct;
import errorHandler.ProductExceptionHandler;
import junit.framework.TestCase;
import main.MainCommandLine;
import model.Product;

/**
 * @author bhupendrabanothe
 *
 */
public class PublicationTest extends TestCase {
	

//	private Product p;

		//Test #: 1
		//Test Objective: add product
		//Inputs: custName = "Product "
		//Expected Output: Exception Message: "true"
	public void testaddproductTest001() throws Exception {
		//Call method under test
		BoProduct testObj = new BoProduct();
//		testObj.initiate_db_conn();
		Product pe = new Product("bhupendra", "0", "newss", "100", "12/11/22");
		boolean res = testObj.insertProductDetails(pe);
		System.out.println(res);
		assertEquals(false, res);	
		
	}
	
	//Test #: 2
	//Test Objective: validateProductNamet
	//Inputs: custName = "prod "s
	//Expected Output: Exception Message: "Product Name does not meet minimum length requirements"
	public void testproductValidateName002() {
		
		try {
			//Call method under test
			Product.validateProductName("prod");
			fail("Exception expected");
		}
		catch (ProductExceptionHandler e) {
			assertEquals("Product Name does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	//Test #: 2
	//Test Objective: validateProductNamet
	//Inputs: custName = "prod "
	//Expected Output: Exception Message: "Product Name does not meet minimum length requirements"
	public void testproductValidateName0010() {
		
		try {
			//Call method under test
			Product.validateProductName("prodsdgfdgdfhfghfghfghgffggfhgfghfghfhfghfghfghfghfghfghfghfghgfhfghfghfghfghfghghfg");
			fail("Exception expected");
		}
		catch (ProductExceptionHandler e) {
			assertEquals("Product Name does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	
		//Test #: 3
		//Test Objective: test product Validate Name003
		//Inputs: custName = " "
		//Expected Output: Exception Message: "ProductName Name NOT specifieds"
	public void testproductValidateName003() {
		
		try {
			//Call method under test
			Product.validateProductName("");
			fail("Exception expected");
		}
		catch (ProductExceptionHandler e) {
			System.out.println(e+"");
			assertEquals("productName Name NOT specified", e.getMessage());	
		}
	}
	
	//Test #: 4
	//Test Objective: test Author Validate Name
	//Inputs: custName = "name "
	//Expected Output: Exception Message: "Product Author Name does not meet minimum length requirements"
	
	public void testTypeValidateName004() {
		
		try {
			//Call method under test
			Product.validaterTypeName("news");
			fail("Exception expected");
		}
		catch (ProductExceptionHandler e) {
			assertEquals("Product type Name does not meet minimum length requirements", e.getMessage());	
		}
	}
	
	//Test #: 5
	//Test Objective: Validate price 
	//Inputs: custName = " 0"
	//Expected Output: Exception Message: "Price is less than or equal to 0"
	
	public void testPriceValidate005() {
		
		try {
			//Call method under test
			Product.validatePrice(0);
			fail("Exception expected");
		}
		catch (ProductExceptionHandler e) {
			assertEquals("Price is less than or equal to 0", e.getMessage());	
		}
	}
	
	//Test #: 5
		//Test Objective: Validate price 
		//Inputs: custName = " 0"
		//Expected Output: Exception Message: "Price is less than or equal to 0"
		
		public void testPriceValidate0011() {
			
			try {
				//Call method under test
				Product.validaterTypeName(" ");
				fail("Exception expected");
			}
			catch (ProductExceptionHandler e) {
				assertEquals("product Type NOT specified", e.getMessage());	
			}
		}
		
		public void testPriceValidate0012() {
			
			try {
				//Call method under test
				Product.validaterTypeName("dhgshjfshjdvhsbdfhsdbfjbshdbvhsdfhsbdhbfhdsbhbdshbfghsbhfbshdfhsdfhshjdfhbsdfhsdhjfbhdsbfhsd");
				fail("Exception expected");
			}
			catch (ProductExceptionHandler e) {
				assertEquals("Product type Name does not meet minimum length requirements", e.getMessage());	
			}
		}
		
	
	
	//Test #: 6
	//Test Objective: Validate Stock
	//Inputs: custName = " 0"
	//Expected Output: Exception Message: "stock is less than or equal to 0"
	
	public void testStockValidate006() {
		
		try {
			//Call method under test
			Product.validateStock(0);
//			fail("Exception expected");
		}
		catch (ProductExceptionHandler e) {
			assertEquals("stock is less than or equal to 0", e.getMessage());	
		}
	}
	
	//Test #: 5
		//Test Objective: Validate price 
		//Inputs: custName = " 0"
		//Expected Output: Exception Message: "Price is less than or equal to 0"
		
		public void testPriceValidate007() {
			
			try {
				//Call method under test
				Product.validatePrice(1200);
				//fail("Exception expected");
			}
			catch (ProductExceptionHandler e) {
				assertEquals("Price is not exceeds more than 1000.", e.getMessage());	
			}
		}
		
		
		public void testStockValidate008() {
			
			try {
				//Call method under test
				Product.validateStock(12200);
			
//				fail("Exception expected");
			}
			catch (ProductExceptionHandler e) {
				assertEquals("stock is not exceeds more than 1000.", e.getMessage());	
			}
		}
		
		
		//Test #: 1
		//Test Objective: add product
		//Inputs: custName = "Product "
		//Expected Output: Exception Message: "true"
	public void testaddproductTest009() throws Exception {
		//Call method under test
		BoProduct testObj = new BoProduct();
		testObj.initiate_db_conn();
		Product pe = new Product("bhupendra", "122", "e-News", "900", "12/11/22");
		boolean res = testObj.insertProductDetails(pe);
		System.out.println(res);
		assertEquals(true, res);	
		
	}
	
	
	public void testUpadateproductTest009() throws Exception {
		//Call method under test
		BoProduct testObj = new BoProduct();
//		testObj.initiate_db_conn();
		Product pe = new Product("bhupendra", "122", "e-News", "900", "12/11/22");
		boolean res = testObj.updateProductById(pe,1);
		System.out.println(res);
		assertEquals(true, res);	
		
	}
	
	public void testupdateproductTest0010() throws Exception {
		//Call method under test
		BoProduct testObj = new BoProduct();
		testObj.initiate_db_conn();
		Product pe = new Product("bhupendra", "122", "e-News", "900", "12/11/22");
		boolean res = testObj.updateProductById(pe,1);
		System.out.println(res);
		assertEquals(true, res);	
		
	}
	
	public void testdeleteproductTest0011() throws Exception {
		//Call method under test
		BoProduct testObj = new BoProduct();
		//testObj.initiate_db_conn();
		Product pe = new Product("bhupendra", "122", "e-News", "900", "12/11/22");
		boolean res = testObj.deleteProductById(2);
		System.out.println(res);
		assertEquals(true, res);	
		
	}
	public void testdeleteproductTest0012() throws Exception {
		//Call method under test
		BoProduct testObj = new BoProduct();
		testObj.initiate_db_conn();
		Product pe = new Product("bhupendra", "122", "e-News", "900", "12/11/22");
		boolean res = testObj.deleteProductById(2);
		System.out.println(res);
		assertEquals(true, res);	
		
	}
	
	public void testDisplayproductTest0012() throws Exception {
		//Call method under test
		BoProduct testObj = new BoProduct();
		testObj.initiate_db_conn();
		ResultSet resultSet = null;
		Product pe = new Product("bhupendra", "122", "e-News", "900", "12/11/22");
		ResultSet res = testObj.retrieveAllProductAccounts();
		System.out.println(res);
		assertEquals(resultSet, res);	
		
	}
	
	

}
