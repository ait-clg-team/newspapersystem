package bo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import model.Deliveryperson;
import model.Order;
import model.Product;

public class OrderBO {
	
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	
	
	public void initiate_db_conn() throws Exception
	{
		
		try
		{
			// Load the JConnector Driver
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/newsagent?serverTimezone=GMT";
			// Connect to DB using DB URL, Username and password
			try {
				connect = DriverManager.getConnection(url, "root", "root12345");
				System.out.println("pass: connect to database\n");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
	
	public boolean insertOrderDetails(Order o) {
		
		boolean insertSucessfull = true;
		
		//Add Code here to call embedded SQL to insert Customer into DB
	
		try {
		
			preparedStatement = connect.prepareStatement("insert into newsagent.orders values (default, ?, ?, ?,?,?,?,?,?)");
			preparedStatement.setString(1, o.getStatus());
			preparedStatement.setString(2, o.getOrder_date());
			preparedStatement.setString(3, o.getUpdate_date());
			preparedStatement.setString(4, o.getBilling_date());
			preparedStatement.setString(5, o.getCreate_at());
			preparedStatement.setInt(6, o.getCustomer_id());
			preparedStatement.setInt(7, o.getProduct_id());
			preparedStatement.setInt(8, o.getArea_id());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (Exception e) {
			System.out.println(""+e);
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertCustomerDetailsAccount
	
	
public boolean updateOrderDetailsById(Order o,int orderId) {
		
		boolean updateSucessfull = true;
		
		//Add Code here to call embedded SQL to insert Customer into DB
	
		try {
		
			preparedStatement = connect.prepareStatement("update newsagent.product set status=? where id = " + orderId);
			preparedStatement.setString(1, o.getStatus());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (Exception e) {
			System.out.println(""+e);
			updateSucessfull = false;
		}
	
		return updateSucessfull;
		
	}// end insertCustomerDetailsAccount
	
	public boolean deleteOrderById(int orderID) {

		boolean deleteSucessfull = true;
		
		//Add Code here to call embedded SQL to insert Customer into DB
		
		try {
			
			//Create prepared statement to issue SQL query to the database
			if (orderID == -99)
				//Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from newsagent.c");
			else
				//Delete a particular Customer
				preparedStatement = connect.prepareStatement("delete from newsagent.orders where id = " + orderID);
			preparedStatement.executeUpdate();
		 
		}
		catch (Exception e) {
			deleteSucessfull = false;
		}
		
		return deleteSucessfull;
		
	}
	
	
	public ResultSet retrieveAllOrder() {
		
		//Add Code here to call embedded SQL to view Customer Details
	
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.orders");
		
		}
		catch (Exception e) {
			resultSet = null;
			System.out.println(e+"ssss");
		}
		return resultSet;
	}




}
