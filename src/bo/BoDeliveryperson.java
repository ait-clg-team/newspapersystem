package bo;

import java.sql.*;

import model.DeliveryArea;
import model.Deliveryperson;


public class BoDeliveryperson {
	
	
	
//	public boolean addProduct(String name) throws Exception  {
//		 //db connect
//		Database db = new Database();
//		db.initiate_db_conn();
//		 
//		throw new Exception("no Product code");
//
//	}
	
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	
	
	public void initiate_db_conn() throws Exception
	{
		
		try
		{
			// Load the JConnector Driver
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/newsagent";
			// Connect to DB using DB URL, Username and password
			try {
				connect = DriverManager.getConnection(url, "root", "root12345");
				System.out.println("pass: connect to database delivery person\n");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
	
	public boolean insertDeliverypersonDetails(Deliveryperson d) {
		
		boolean insertSucessfull = true;
		
		//Add Code here to call embedded SQL to insert Customer into DB
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			
//			preparedStatement = connect.prepareStatement("INSERT INTO `newsagent`.`product`( `name`, `price`,`type`,`status`, `created_at`) VALUES (`bhhddddd`, `123`, `dsfdsdf`, `sdfsdf`, `sdsfsd`)");
			preparedStatement = connect.prepareStatement("insert into newsagent.deliveryperson values (default,?, ?, ?, ?,?,?)");
			preparedStatement.setString(1, d.addDPname());
			preparedStatement.setString(2, d.addDPPhoneNumber());
			preparedStatement.setString(3, d.addDPaddress());
			preparedStatement.setBoolean(4, d.addDPActive());
			preparedStatement.setInt(5, d.addDPsubPlan());
			preparedStatement.setString(6, d.addDPcreatedTime());	
		
			preparedStatement.executeUpdate();
		
	 
		}
		catch (Exception e) {
			System.out.println(""+e);
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end 
	
public boolean updateDeliverypersonDetails(Deliveryperson d) {
		
		boolean updateSucessfull = true;
		
		//Add Code here to call embedded SQL to insert Customer into DB
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			
//			preparedStatement = connect.prepareStatement("INSERT INTO `newsagent`.`product`( `name`, `price`,`type`,`status`, `created_at`) VALUES (`bhhddddd`, `123`, `dsfdsdf`, `sdfsdf`, `sdsfsd`)");
			preparedStatement = connect.prepareStatement("update newsagent.deliveryperson set values (default,?, ?, ?, ?,?,?)");
			preparedStatement.setString(1, d.addDPname());
			preparedStatement.setString(2, d.addDPPhoneNumber());
			preparedStatement.setString(3, d.addDPaddress());
			preparedStatement.setBoolean(4, d.addDPActive());
			preparedStatement.setInt(5, d.addDPsubPlan());
			preparedStatement.setString(6, d.addDPcreatedTime());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (Exception e) {
			System.out.println(""+e);
			updateSucessfull = false;
		}
	
		return updateSucessfull;
		
	}// end insertCustomerDetailsAccount

	public boolean deleteDeliverypersonDetails(int DPid) {
	
		boolean deleteSucessfull = true;

		try {
			
			if (DPid == -99)
								//Delete all entries in Table
								preparedStatement = connect.prepareStatement("delete from newsagent2021.customer");
							else
								//Delete a particular Customer
								preparedStatement = connect.prepareStatement("delete from newsagent2021.customer where id = " + DPid);
							preparedStatement.executeUpdate();
	
		}
		catch (Exception e) {
			System.out.println(""+e);
			deleteSucessfull = false;
		}
	
		return deleteSucessfull;
		
	}

	public boolean deleteDeliveryAreaDetails(DeliveryArea dAObj) {
		// TODO Auto-generated method stub
		return false;
	}
	

	public ResultSet retrieveAllProductAccounts() {
		
		//Add Code here to call embedded SQL to view Customer Details
	
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.deliveryperson");
		
		}
		catch (Exception e) {
			resultSet = null;
			System.out.println(e+"ssss");
		}
		return resultSet;
	}

	public boolean insertDeliveryAreaDetails(DeliveryArea dAObj) {
		// TODO Auto-generated method stub
		return false;
	}

	


}

