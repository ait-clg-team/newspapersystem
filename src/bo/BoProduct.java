package bo;

import java.sql.*;

import model.Product;


public class BoProduct {
	
	
	
//	public boolean addProduct(String name) throws Exception  {
//		 //db connect
//		Database db = new Database();
//		db.initiate_db_conn();
//		 
//		throw new Exception("no Product code");
//
//	}
	
	
	
	
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	
	
	public void initiate_db_conn() throws Exception
	{
		
		try
		{
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/newsagent";
			// Connect to DB using DB URL, Username and password
			try {
				connect = DriverManager.getConnection(url, "root", "root12345");
				System.out.println("pass: connect to database product\n");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
	
	public boolean insertProductDetails(Product p) {
		
		boolean insertSucessfull = true;
		
		//Add Code here to call embedded SQL to insert Customer into DB
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			
//			preparedStatement = connect.prepareStatement("INSERT INTO `newsagent`.`product`( `name`, `price`,`type`,`status`, `created_at`) VALUES (`bhhddddd`, `123`, `dsfdsdf`, `sdfsdf`, `sdsfsd`)");
			preparedStatement = connect.prepareStatement("insert into newsagent.product values (default, ?, ?, ?,?,?)");
			preparedStatement.setString(1, p.getName());
			preparedStatement.setString(2, p.getPrice());
			preparedStatement.setString(3, p.getType());
			preparedStatement.setString(4, p.getStauts());
			preparedStatement.setString(5, p.getDate());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (Exception e) {
			System.out.println(""+e);
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertCustomerDetailsAccount
	
	
	public boolean updateProductById(Product p, int productId) {
		
		boolean updateSucessfull = true;
		
		//Add Code here to call embedded SQL to insert Customer into DB
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			
//			preparedStatement = connect.prepareStatement("INSERT INTO `newsagent`.`product`( `name`, `price`,`type`,`status`, `created_at`) VALUES (`bhhddddd`, `123`, `dsfdsdf`, `sdfsdf`, `sdsfsd`)");
			preparedStatement = connect.prepareStatement("update newsagent.product set name=? where id = " + productId);
			preparedStatement.setString(1, p.getName());
//			preparedStatement.setString(2, p.getPrice());
//			preparedStatement.setString(3, p.getType());
//			preparedStatement.setString(4, p.getStauts()); 
//			preparedStatement.setString(5, p.getDate());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (Exception e) {
			System.out.println(""+e);
			updateSucessfull = false;
		}
	
		return updateSucessfull;
		
	}// end insertCustomerDetailsAccount

	

	public ResultSet retrieveAllProductAccounts() {
		
		//Add Code here to call embedded SQL to view Customer Details
		
	
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.product");
		
		}
		catch (Exception e) {
			resultSet = null;
			System.out.println(e+"ssss");
		}
		return resultSet;
	}
	
	public boolean deleteProductById(int producttID) {

		boolean deleteSucessfull = true;
		
		//Add Code here to call embedded SQL to insert Customer into DB
		
		try {
			
			//Create prepared statement to issue SQL query to the database
			if (producttID == -99)
				//Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from newsagent.product");
			else
				//Delete a particular Customer
				preparedStatement = connect.prepareStatement("delete from newsagent.product where id = " + producttID);
			preparedStatement.executeUpdate();
		 
		}
		catch (Exception e) {
			deleteSucessfull = false;
		}
		
		return deleteSucessfull;
		
	}


}
