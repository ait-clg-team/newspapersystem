package bo;

import java.sql.*;

import model.DeliveryArea;


public class BoDeliveryArea {
	
	
	
//	public boolean addProduct(String name) throws Exception  {
//		 //db connect
//		Database db = new Database();
//		db.initiate_db_conn();
//		 
//		throw new Exception("no Product code");
//
//	}
	
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	
	
	public void initiate_db_conn() throws Exception
	{
		
		try
		{
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/newsagent?serverTimezone=GMT";
			// Connect to DB using DB URL, Username and password
			try {
				connect = DriverManager.getConnection(url, "root", "root");
				System.out.println("pass: connect to database\n");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
	
	public boolean insertDeliveryAreaDetails(DeliveryArea da) {
		
		boolean insertSucessfull = true;
		
		//Add Code here to call embedded SQL to insert Customer into DB
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			
//			preparedStatement = connect.prepareStatement("INSERT INTO `newsagent`.`product`( `name`, `price`,`type`,`status`, `created_at`) VALUES (`bhhddddd`, `123`, `dsfdsdf`, `sdfsdf`, `sdsfsd`)");
			preparedStatement = connect.prepareStatement("insert into newsagent.deliveryarea values (default,?, ?, ?)");
			preparedStatement.setString(1, da.getDA_country());
			preparedStatement.setString(2, da.getDA_town());
			preparedStatement.setInt(3, da.getDA_id());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (Exception e) {
			System.out.println(""+e);
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertCustomerDetailsAccount
	
public boolean updateDeliveryAreaDetails(DeliveryArea da) {
		
		boolean updateSucessfull = true;
		
		//Add Code here to call embedded SQL to insert Customer into DB
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			
//			preparedStatement = connect.prepareStatement("INSERT INTO `newsagent`.`product`( `name`, `price`,`type`,`status`, `created_at`) VALUES (`bhhddddd`, `123`, `dsfdsdf`, `sdfsdf`, `sdsfsd`)");
			preparedStatement = connect.prepareStatement("update newsagent.deliveryarea set values (default,?, ?, ?)");
			preparedStatement.setString(1, da.getDA_country());
			preparedStatement.setString(2, da.getDA_town());
			preparedStatement.setInt(3, da.getDA_id());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (Exception e) {
			System.out.println(""+e);
			updateSucessfull = false;
		}
	
		return updateSucessfull;
		
	}// end insertCustomerDetailsAccount

	

	public ResultSet retrieveAllProductAccounts() {
		
		//Add Code here to call embedded SQL to view Customer Details
	
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.Deliveryperson");
		
		}
		catch (Exception e) {
			resultSet = null;
			System.out.println(e+"ssss");
		}
		return resultSet;
	}


}

