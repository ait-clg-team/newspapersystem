package bo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

import model.Customer;

import java.sql.ResultSet;

public class CustomerSqlAcess {
	
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	
	final private String host ="localhost:3306";
	final private String user = "root";
	final private String password = "root!20";
	
	
	public void initiate_db_conn() throws Exception {
		
		try
		{
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/newsagent";
			// Connect to DB using DB URL, Username and password
			try {
				connect = DriverManager.getConnection(url, "root", "root12345");
				System.out.println("pass: connect to database customer\n");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
		
	}	

	public boolean insertCustomerDetailsAccount(Customer c) {
	
		boolean insertSucessfull = true;
	
		//Add Code here to call embedded SQL to insert Customer into DB
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("insert into newsagent.customer values (default, ?, ?, ?,?,?,?)");
			preparedStatement.setString(1, c.getFullname());
			preparedStatement.setString(2, c.getPhoneNumber());
			preparedStatement.setString(3, c.getAddress());
			preparedStatement.setString(4, c.getIsActive());
			preparedStatement.setString(5, c.getSubPlan());
			preparedStatement.setString(6, c.getCreated_at());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (Exception e) {
			System.out.println(""+e);
			insertSucessfull = false;
			
		}
	
		return insertSucessfull;
		
	}// end insertCustomerDetailsAccount

	public ResultSet retrieveAllCustomerAccounts() {
		
		//Add Code here to call embedded SQL to view Customer Details
	
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from newsagent.customer");
		
		}
		catch (Exception e) {
			resultSet = null;
		}
		return resultSet;
	}
	
	public boolean deleteCustomerById(int custID) {

		boolean deleteSucessfull = true;
		
		//Add Code here to call embedded SQL to insert Customer into DB
		
		try {
			
			//Create prepared statement to issue SQL query to the database
			if (custID == -99)
				//Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from newsagent2021.customer");
			else
				//Delete a particular Customer
				preparedStatement = connect.prepareStatement("delete from newsagent2021.customer where id = " + custID);
			preparedStatement.executeUpdate();
		 
		}
		catch (Exception e) {
			deleteSucessfull = false;
		}
		
		return deleteSucessfull;
		
	}

	
	



}
